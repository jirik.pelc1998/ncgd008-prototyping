using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    public float DayLength;
    public float CurrentTime;
    public float Progress;

    // Update is called once per frame
    void Update()
    {
        CurrentTime += Time.deltaTime;
        if (CurrentTime > DayLength)
            CurrentTime -= DayLength;
        Progress = CurrentTime / DayLength;
        this.transform.localEulerAngles = new Vector3(0, 0, 360 * Progress);
    }


}
