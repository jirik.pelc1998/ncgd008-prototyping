using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldGenerator : MonoBehaviour
{
    public GameObject chunkPrefab;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = -2; i <= 2; i++)
        {
            for (int j = -2; j <= 2; j++)
            {
                var chunk = Instantiate(chunkPrefab, new Vector3(i, 0, j)*ChunkScript.ChunkSize, Quaternion.identity);
                chunk.transform.SetParent(transform, true);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
