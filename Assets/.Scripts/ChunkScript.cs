using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class WorldObjectData
{
    public GameObject Prefab;
    public float Probability;
}

public class ChunkScript : MonoBehaviour
{
    public const int ChunkSize = 16;
    public Vector3 PerlinScale = Vector3.one;
    public Vector2 Perlin2Offset;
    public GameObject CubePrefab;
    public List<WorldObjectData> Variants;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < ChunkSize; i++)
        { 

            for (int j = 0; j < ChunkSize; j++)
            {
                var y = GetCubeY(i, j);
                var cube = GetCube();
                cube.transform.SetParent(transform, true);
                cube.transform.localPosition = new Vector3(i,y,j);
            }
        }
    }

    private GameObject GetCube()
    {
        foreach (var variant in Variants)
        {
            var rnd = Random.Range(0f, 1f);
            if (rnd <= variant.Probability)
            {
                var inst = Instantiate(variant.Prefab);
                var rot = Random.Range(0, 4) * 90;
                inst.transform.localEulerAngles = new Vector3(0, rot, 0);
                return inst;
            }
        }
        return Instantiate(CubePrefab);
    }

    private float GetCubeY(int i, int j)
    {
        var p1 = Mathf.PerlinNoise((transform.position.x + i) * PerlinScale.x, (transform.position.z + j) * PerlinScale.z) * PerlinScale.y;
        var p2 = Mathf.PerlinNoise((transform.position.x + i + Perlin2Offset.x) * PerlinScale.x, (transform.position.z + j + Perlin2Offset.y) * PerlinScale.z) * PerlinScale.y;
        return Mathf.RoundToInt(p1 * p2);
    }
}
